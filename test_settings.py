#!/usr/bin/env python

HELPER_SETTINGS = {
    'SECRET_KEY': 'NOT-EMPTY',
    'DEFAULT_AUTO_FIELD': 'django.db.models.BigAutoField',
}


def run():
    from djangocms_helper import runner
    runner.cms('djangocms_link_manager')


if __name__ == "__main__":
    run()
